/*
 * Homadeus Platform Homadeus Utilities
 * Copyright (C) 2013 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_HEX_UTILS_H
#define HOMADEUS_HEX_UTILS_H

#include <stdint.h>
#include <stdlib.h>

extern const char hex_asc[];
#define hex_asc_lo(x)   hex_asc[((x) & 0x0f)]
#define hex_asc_hi(x)   hex_asc[((x) & 0xf0) >> 4]

int hex_to_bin(char ch);
int bin2hex(char *dst, const uint8_t *src, size_t count);
int hex2bin(uint8_t *dst, const char *src, size_t count);

#endif /* end of include guard: HOMADEUS_HEX_UTILS_H */
