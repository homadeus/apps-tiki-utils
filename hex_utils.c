#include <homadeus/utils/hex.h>

const char hex_asc[] = "0123456789abcdef";

int hex_to_bin(char ch)
{
        if ((ch >= '0') && (ch <= '9'))
                return ch - '0';
        if ((ch >= 'a') && (ch <= 'f'))
                return ch - 'a' + 10;
        if ((ch >= 'A') && (ch <= 'F'))
                return ch - 'A' + 10;
        return -1;
}

int hex2bin(uint8_t *dst, const char *src, size_t count)
{
        while (count--) {
                int hi = hex_to_bin(*src++);
                int lo = hex_to_bin(*src++);

                if ((hi < 0) || (lo < 0))
                        return -1;

                *dst++ = (hi << 4) | lo;
        }
        return 0;
}
int bin2hex(char *dst, const uint8_t *src, size_t count)
{
        while (count--) {
                *dst++ = hex_asc_hi(*src);
                *dst++ = hex_asc_lo(*src);
                src++;
        }
        return 0;
}
